from flask import Flask, jsonify

import pymongo
from pymongo import MongoClient

app = Flask(__name__)

def get_db():
    client = MongoClient(
        host="test_mongo",
        port=27017,
        user="zloi",
        password="zloi",
        authSource="admin"
    )
    return client.test_data


@app.route("/")
def index():
    return "This is flask index page"

@app.route("/data")
def data():
    db=""
    try:
        db = get_db()
        _animals = db.animal_tb.find()
        animals = [{"id": animal["id"], "name": animal["name"], "type": animal["type"]} for animal in _animals]
        return jsonify({"animals": animals})
    except:
        pass
    finally:
        if type(db)==MongoClient:
            db.close()



if __name__ == "__main__":
    app.run()